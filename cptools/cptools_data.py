import json
from ndict import NestedDict
from .cptools_constants import cpConstants
from .cptools_exceptions import cpException


class cpData():
	"""
	Generic Check Point data.

	Instance attributes:
	  - 'data': List of items in format: [{'type': <type>, 'body': {<item_json_body>}}, {<item2>}, ...]
	  - 'uids': Dictionary containig UID to name associations. Used only when UIDs are included in items.
	"""


	def __init__(self, constants=cpConstants()):
		"""
		Constructor.

		:param contants: (optional) cpConstants instance.
						 By default a new one is created.
		:return: cpData instance.
		"""

		self.data = []				# Data dictionary.
		self.uids = {}				# UIDs dictionary. UIDs added only if they exist.
		self.constants = constants	# cpConstants instance.


	def __str__(self):
		"""
		Print instance. Format: {'data': self.data, 'uids': self.uids}
		"""
		return json.dumps({'data': self.data, 'uids': self.uids}, indent=4)


	def __repr__(self):
		"""
		Reproduce instance. Format: {'data': self.data, 'uids': self.uids}
		"""
		return repr({'data': self.data, 'uids': self.uids})


	def add(self, items):
		"""
		Add new items. Each item should at least have 'type' param defined. Otherwise an excpetion is raised.
		Items are saved in {'type': <type>, 'body': {<body>}} format.

		:param items: Single item or list of items (dictionaries defining each item).
		"""

		if not isinstance(items, list):
			items = [items]

		# Process each item.
		for item in items:

			# Proceed only if an item has a 'type' param defined. Otherwise raise an exception.
			if 'type' in item:
				self.data.append({'type': item['type'], 'body': {key: item[key] for key in item if key != 'type'}})
			else:
				raise cpException('No type defined in:\n' + repr(item))


	def uids2names(self, uids, replace_missing=False, replacement=None):
		"""
		Translate UIDs to names based on the content of 'uids' attribute.
		If particual uid is not found it is left unchanged.
		Translated names are also chekced against the cpConstants.translations dict. and modified if found.

		:param uids: Single UID (str) or a list of UIDs.
		:param replace_missing: (optional) Replaced missing UIDs (True/False)?
								Default value: False
		:param replacement: (optional) Replacement value (works with 'replace_missing').
							Default value: 'None' (in case of a list particular item is deleted).
		:return: (<single-name/list-of-names>, [<list-of-missed-uids>]).
		"""

		names_list = []		# List of names.
		miss_list = []		# List of UIDs that can't be translated.
		val_list = True		# 'uids' is a list?

		if not isinstance(uids, list):
			uids = [uids]
			val_list = False

		# Process each item in a list.
		for uid in uids:

			# UID found.
			if uid in self.uids:
				#names_list.append(self.uids[uid])
				names_list.append(self.constants.translations.get(self.uids[uid], self.uids[uid]))

			# Name found instead of UID - ignore.
			elif uid in self.uids.values():
				names_list.append(uid)

			# UID not found.
			else:

				# Replace missing.
				if replace_missing:
					if replacement is None:
						miss_list.append(uid)
					else:
						miss_list.append(uid)
						names_list.append(replacement)

				# Do not replace - return original.
				else:
					names_list.append(uid)
					miss_list.append(uid)

		# Return result.
		if val_list:
			return (names_list, miss_list)
		else:
			if names_list:
				return (names_list[0], miss_list)
			else:
				return (None, miss_list)


	def translate(self, replace_missing=False, replacement=None):
		"""
		Translate uids to names inside items (e.g. groups, rules) based on the 'uids' attribute content.
		If particual uid is not found it is left unchanged.

		:param replace_missing: (optional) Replaced missing UIDs (True/False)?
								Default value: False
		:param replacement: (optional) Replacement value (works with 'replace_missing').
							Default value: 'None'
		:return: List of UIDs that couldn't be translated.
		"""

		miss_set = set()

		# Process all rules.
		for item in self.data:

			nested_body = NestedDict(item['body'])

			# Check defined params.
			for param in self.constants.ref_dict:

				# Param used for specific type.
				if item['type'] in self.constants.ref_dict[param]:

					# Param must be within the item body. Otherwise do nothing.
					if param in nested_body:

						# Translate values of a single param.
						(nested_body[param], miss) = self.uids2names(nested_body[param], replace_missing, replacement)

						# Found missed UID(s).
						if miss:
							miss_set.update(miss)

			# Update current rule.
			item['body'] = nested_body.dict

		return list(miss_set)


	def search(self, search_list=[], attr_dict={}):
		"""
		Search for items that meet specific criteria.
		First items are filtered by their attributes, then by body parameters.
		If no arguments are used, simply all items are returned.

		:param search_list: (optional) Search list in [{<param/nested-param-tuple>: <value>/[<values_list>]}, {...}] format.
							Example: [{'name': ['host1', 'host2'], 'color': 'red', ('nat', 'enabled'): 'yes'}].
							Between statements (dictionaries) in a list there is logical OR.
							Within a single statement (dictionary), between values in a list there is logical OR.
							Within a single statement (dictionary), between dictionary keys there is logical AND.
		:praram attr_dict: (optional) Dictionary in {<attribute>: <value>/[values_list>]} format.
						   Between values in a list there is logical OR.
						   Between dictionary keys there is logical AND.
		:return: List of items (dicts) matching search criteria.
		"""

		search_results = []		# Search result.
		search_attributes = []	# Intermediate search result after filtering attributes.

		# Attributes filter set.
		if attr_dict:

			# Process all items.
			for item in self.data:

				match = True

				# Process all attributes.
				# If at least one attribute doesn't match, set 'match' var. to False.
				for attr in attr_dict:
					vals_list = attr_dict[attr] if isinstance(attr_dict[attr], list) else [attr_dict[attr]]
					if not item.get(attr) in vals_list:
						match = False

				# All attributes match add to the intermediate search result.
				if match:
					search_attributes.append(item)

		# Attributes filter not set - use all items for the next step.
		else:
			search_attributes = self.data

		# Body params filter is set.
		if search_list:

			# Process each item from  the intermediate search result.
			for item in search_attributes:

				nested_item = NestedDict(item['body'])		# Convert body to a NestedDict object.
				match_list = []

				for search_dict in search_list:

					match = True

					# Process keys in the search dict.
					# All keys must exist and have a match to put an item in the results list.
					for keys in search_dict:

						# Item contains a key - check values.
						if keys in nested_item:

							# Values to be checked.
							vals_list = [search_dict[keys]]	if isinstance(search_dict[keys], (str, int)) else search_dict[keys]

							# Value is a list.
							if isinstance(nested_item[keys], list):
								if not any(val in vals_list for val in nested_item[keys]):
									match = False

							# Value is a string or int.
							else:
								if not nested_item[keys] in vals_list:
									match = False

						# Item doesn't contain a key - no match.
						else:
							match = False

					# Matched item.
					match_list.append(match)

				if any(match_list):
					search_results.append(item)

		# Body params filter is not set - return items only after filtering them by attributes.
		else:
			search_results = search_attributes

		# Return a list of items.
		return search_results


	@staticmethod
	def replace(body, replace_list):
		"""
		Static method. Replace values in a body of an item.

		:param body: Dictionary containing item's body.
		:param replace_list: Replace list in [{'key': <param/nested-param-tuple>, 'val_before': <value/None>, 'val_after': <value/None>}, {...}] format.
							 Example: [{'key': 'color', 'val_before': 'blue', 'val_after': 'red'}, {'key': ('nat', 'enabled'), 'val_before': 'yes', 'val_after': 'no'}].
							 If 'val_before' is 'None', a new value will be appended (list) or it will replace the current value (string or int).
							 If 'val_after' is 'None', a value will be removed (list) or set to 'None' (string or int).
		:return: A list of boolean values indicating that specific field defined in the 'replace_list' was replaced.
		"""

		replace_result = []					# Replace result.
		nested_body = NestedDict(body)		# Body in NestedDict format.

		# Process replace list.
		for replace_dict in replace_list:

			key = replace_dict['key']

			# Value-before is set.
			if replace_dict['val_before']:

				# Key exists.
				if key in nested_body:

					# Value is a list.
					if isinstance(nested_body[key], list):

						# Value-before is within the list - replace.
						# If value-after is None, only remove.
						if replace_dict['val_before'] in nested_body[key]:

							# Value-after is set.
							if replace_dict['val_after']:

								# Value-after already in the values list - only remove value to avoid duplicates.
								if replace_dict['val_after'] in nested_body[key]:
									replace_result.append(True)
									nested_body[key].remove(replace_dict['val_before'])

								# Value-after not in the values list - replace
								else:
									replace_result.append(True)
									nested_body[key].remove(replace_dict['val_before'])
									nested_body[key].append(replace_dict['val_after'])

							# Value-after is not set - remove.
							else:
								replace_result.append(True)
								nested_body[key].remove(replace_dict['val_before'])

						# Velue-before not in the list - do nothing.
						else:
							replace_result.append(False)

					# Value is a string or int.
					else:

						# Value-before = item value - replace.
						# If value-after is None, only remove.
						if replace_dict['val_before'] == nested_body[key]:
							replace_result.append(True)
							nested_body[key] = replace_dict['val_after'] if replace_dict['val_after'] else None

						# Value-before != item value - do nothing.
						else:
							replace_result.append(False)

				# Key doesn't exist. Do nothing.
				else:
					replace_result.append(False)

			# Value-before is not set - only add value.
			else:

				# Value is a list.
				if isinstance(nested_body[key], list):

					# Value already in a value list - do nothing to avoid duplicates.
					if replace_dict['val_after'] in nested_body[key]:
						replace_result.append(False)

					# Value not in the list - add it.
					else:
						nested_body[key].append(replace_dict['val_after'])
						replace_result.append(True)

				# Value is a string or int.
				else:

					# New value is the same as the old one - do nothing.
					if replace_dict['val_after'] == nested_body[key]:
						replace_result.append(False)

					# New value is different from the old one - update.
					else:
						nested_body[key] = replace_dict['val_after']
						replace_result.append(True)

		# Update body if at least one replacement was done.
		if any(result for result in replace_result):
			body = nested_body.dict

		return replace_result


	@staticmethod
	def crop(body, param_list=[], skip_none=True):
		"""
		Static method. Crop body content based on the list of required parameters.
		By default functions strips all empty params (with '', [], {}, None values).

		:param body: Dictionary containing item's body definition.
		:param param_list: (optional) List of (nested) parameters that need to be displayed, e.g. ['name', ('nat', 'enabled')].
						   If not specified, all parameters within body are returned.
						   Non-existing parameters are ignored.
		:param skip_none: (optional) True/False. Skip params with empty values.
						  Default value: True.
		:return: Item's body dict.
		"""

		nested_body = NestedDict(body)
		return_body = NestedDict()

		if not param_list:
			param_list = nested_body.nestedkeys()

		for nkey in param_list:
			if nkey in nested_body:
				if not skip_none or not nested_body[nkey] in [[], {}, '', None]:
					return_body[nkey] = nested_body[nkey]
		return return_body.dict


	@staticmethod
	def compare(body1, body2, cmp_list=[], skip_none=True):
		"""
		Static method. Compare body1 against body2.
		By default all body2 keys are checked. You can also use an auxilary list of keys to be checked.
		If param is not found in body2 it is not checked. But if body2 param doesn't exists in body1, 'False' is returned.
		If value is a list, method checks if both bodies contain the same items in these lists (order can be different).
		In other cases simple '==' comparison is used.

		:param body1: Body (dict) of the first item.
		:param body2: Body (doct) of the second (reference) item.
		:param cmp_list: (optional) List of nested keys to be checked. If specific key is not used in body2 it is ignored.
		 				 By default the list it empty. in such case check all keys in body2.
		:param skip_none: (optional) True/False. Skip params with empty values.
						  Default value: True.
		:return: True/False.
		"""

		# body1 and body2 not empty - proceed.
		if body1 and body2:

			match = True
			nested_body1 = NestedDict(cpData.crop(body1)) if skip_none else NestedDict(body1)
			nested_body2 = NestedDict(cpData.crop(body2)) if skip_none else NestedDict(body2)

			# Nested keys to be checked.
			nested_keys = cmp_list if cmp_list else nested_body2.nestedkeys()

			# Process all keys.
			for nkey in nested_keys:

				# Proceed if key is in body2. Otherwise ignore key.
				if nkey in nested_body2:

					# Key is in body1.
					if nkey in nested_body1:

						# Value in body2 is a list.
						if isinstance(nested_body2[nkey], list):

							# Value in body1 is also a list - continue.
							if isinstance(nested_body1[nkey], list):

								# List length in body1 and body2 is the same - continue.
								if len(nested_body2[nkey]) == len(nested_body1[nkey]):

									# Check values. If they are not the same - no match.
									for val in nested_body1[nkey]:
										if not val in nested_body2[nkey]:
											match = False

								# Length is not the same - no match.
								else:
									match = False

							# Value in body1 is not a list - no match.
							else:
								match = False

						# Value in body2 is not a list.
						else:
							if nested_body2[nkey] != nested_body1[nkey]:
								match = False

					# Key is not in body1 - no match.
					else:
						match = False

			# Return True/False.
			return match

		# body1 and body2 empty - return False.
		else:
			return False




class cpRulebase(cpData):
	"""
	Check Point rulebases manipulation.

	Instance attributes:
	  - 'data': List of rules/sections in format: [{'type': <type>, 'rule-number': <number-int>, 'container': <layer/package>, 'body': {<item_json_body>}}, {<rule2>}, ...]
	  - 'uids': Dictionary containig UID to name associations. Used only when UIDs are included in items.

	Each rule can be identified by its number stored in the 'rule-number' attribute. This number is calculated dynamically when a rule is added, strarting from '1'.
	Sections also have 'rule-number' attribute which refers to the number of the next rule below.

	At the moment inserting rules in the middle of the rulebase is not supported (rule numbers are not re-calculated is such case).
	New rules are added always at the end of the rulebase.
	"""


	def __init__(self, items=None, container=None, constants=cpConstants()):
		"""
		Constructor.

		:param items: (optional) Item or list of items in [{<item1>}, {<item2}, ...] format.
		:param container: (optional) Layer or package depending on the item type.
						  If not defined, container is searched within the item dict. If not found an exception is raised.
		:param contants: (optional) cpConstants instance.
						 By default a new one is created.
		:return: cpRulebase instance.
		"""

		cpData.__init__(self, constants)
		if items:
			self.add(items, container)


	def rules_count(self, container):
		"""
		Return number of rules in a container.

		:param container: Name of the container (Layer/Package).
		:return: Number of rules (int).
		"""

		count = 0
		for item in self.data:
			if item['container'] == container and item['type'] not in ['access-section', 'nat-section']:
				count += 1
		return count


	def add(self, items, container=None):
		"""
		Add new rules/sections.

		:param items: Item or list of items in [{<item1>}, {<item2>}, ...] format.
		:param container: (optional) Layer or package depending on the item type. Overrides information found in items.
						  If not defined, container is searched within the item dict. If not found an exception is raised.
		"""

		if not isinstance(items, list):
			items = [items]

		# Process each item.
		for item in items:

			# Set type or raise an exception.
			if 'type' in item:
				type = item['type']
				cont_type = 'package' if type in ['nat-rule', 'nat-section'] else 'layer'
			else:
				raise cpException('No type defined in:\n' + repr(item))

			if container:
				cont_name = container
			elif cont_type in item:
				#cont_name = container if container else item[cont_type]		# Excpetion add - no containter...
				cont_name = item[cont_type]
			else:
				raise cpException('Layer or package not defined in:\n' + repr(item))

			self.data.append({'type': type, 'rule-number': self.rules_count(cont_name) + 1, 'container': cont_name, 'body': {key: item[key] for key in item if not key in ['type', 'rule-number']}})
			self.data[-1]['body'][cont_type] = cont_name


	def rule(self, container, position, include_sections=True):
		"""
		Return a single rule based on its number and container name.
		Result is presented as a list which includes the rule and (optionally) associated section.
		If a rule doesn't exist, an empty list is returned.

		:param container: Name of the container (Layer/Package).
		:param position: Rule number (int) as defined in attribute 'rule-number'.
		:param include_sections: (optional) Include section title if associated with the rule (yes by default).
		:return: List of rules/sections.
		"""

		result = []
		for item in self.data:
			if item['container'] == container and item['rule-number'] == position:
				if item['type'] in ['access-section', 'nat-section']:
					if include_sections:
						result.append(item)
				else:
					result.append(item)
		return result


	def rules(self, container, positions=[], include_sections=True):
		"""
		Return a list of rules based on their numbers and container name.
		Non-existing rules are omitted.

		:param container: Name of the container (Layer/Package).
		:param positions: (optional) List of positions (single int. - single rule; tuple with two ints. - range of rules.).
						  If no positions are specified, all rules from a container are returned.
		:param include_sections: (optional) Include section title if associated with the rule (yes by default).
		:return: List of rules/sections.
		"""

		result = []

		if not positions:
			positions = list(range(1, self.rules_count(container) + 1))

		for position in positions:
			if isinstance(position, tuple):
				for range_position in range(position[0], position[1] + 1):
					result += self.rule(container, range_position, include_sections)
			elif isinstance(position, int):
				result += self.rule(container, position, include_sections)

		return result


	def containers(self):
		"""
		Return list of containers used for all rules.

		:return: List of containers.
		"""

		return list({item['container'] for item in self.data})


	def search(self, search_list=[], attr_dict={}):
		"""
		Search for items that meet specific criteria.
		First items are filtered by their attributes, then by body parameters.
		If no arguments are used, simply all items are returned.

		:param search_list: (optional) Search list in [{<param/nested-param-tuple>: <value>/[<values_list>]}, {...}] format.
							Example: [{'name': ['host1', 'host2'], 'color': 'red', ('nat', 'enabled'): 'yes'}].
							Between statements (dictionaries) in a list there is logical OR.
							Within a single statement (dictionary), between values in a list there is logical OR.
							Within a single statement (dictionary), between dictionary keys there is logical AND.
		:praram attr_dict: (optional) Dictionary in {<attribute>: <value>/[values_list>]} format.
						   Between values in a list there is logical OR.
						   Between dictionary keys there is logical AND.
		:return: List of (<container>, <rule-number>) tuples.
		"""

		return [(item['container'], item['rule-number']) for item in cpData.search(self, search_list, attr_dict) if not item['type'] in ['access-section', 'nat-section']]


	def replace(self, container, position, replace_list):
		"""
		Change a specific rule.

		:param container: Layer or package.
		:param position: Rule number (int).
		:param replace_list: Replace list in [{'key': <param/nested-param-tuple>, 'val_before': <value/None>, 'val_after': <value/None>}, {...}] format.
							 Example: [{'key': 'color', 'val_before': 'blue', 'val_after': 'red'}, {'key': ('nat', 'enabled'), 'val_before': 'yes', 'val_after': 'no'}].
							 If 'val_before' is 'None', a new value will be appended (list) or it will replace the current value (string or int).
							 If 'val_after' is 'None', a value will be removed (list) or set to 'None' (string or int).
		:return: A list of boolean values indicating that specific field defined in the 'replace_list' was replaced.
		"""

		return cpData.replace(self.rule(container, position, False)[0]['body'], replace_list)


	def gen_crop(self, container, positions=[], param_list=[], include_sections=True, skip_none=True):
		"""
		Generator function. Crop body of selected rules and sections.

		:param container: Layer or package.
		:param positions: (optional) List of positions (single int. - single rule; tuple with two ints. - range of rules.).
						  If no positions are specified, all rules from a container are returned.
		:param param_list: (optional) List of (nested) parameters that need to be displayed, e.g. ['name', ('nat', 'enabled')].
						   If not specified, default parameters from cpConstants are used.
						   Non-existing parameters are ignored.
		:param include_sections: (optional) Include section titles if associated with rules (True by default).
		:param skip_none: (optional) True/False. Skip params with empty values.
						  Default value: True.
		:yield: (<type>, <body>, <position>) tuple.
		"""

		for item in self.rules(container, positions, include_sections):
			if param_list:
				yield (item['type'], cpData.crop(item['body'], param_list, skip_none), item.get('rule-number'))
			else:
				yield (item['type'], cpData.crop(item['body'], self.constants.params(item['type']), skip_none), item.get('rule-number'))


	def references(self, container, positions=[], include_builtin=False):
		"""
		Return a list of object names/uids referenced in selected rules.

		:param container: Name of the container (Layer/Package).
		:param positions: (optional) List of positions (single int. - single rule; tuple with two ints. - range of rules.).
						  If no positions are specified, all rules from a container are returned.
		:param include_builtin: (optional) True/False - include builtin objects (e.g. 'Any').
								Default value: False.
		:return: List of objects names/uids.
		"""

		ref_set = set()

		# Process rules.
		for item in self.rules(container, positions, False):

			nested_body = NestedDict(item['body'])

			# Process params that contain references.
			for param in self.constants.ref_dict:
				if param in nested_body:
					if isinstance(nested_body[param], list):
						ref_set.update(nested_body[param])
					else:
						ref_set.add(nested_body[param])

		# Return references with or without builtin objects.
		if include_builtin:
			return list(ref_set)
		else:
			return [obj for obj in ref_set if not obj in self.constants.builtin_obj]




class cpObjects(cpData):
	"""
	Check Point objects manipulation.

	Instance attributes:
	  - 'data': List of objects in format: [{'type': <type>, 'body': {<item_json_body>}}, {<object2>}, ...]
	  - 'uids': Dictionary containig UID to name associations. Used only when UIDs are included in items.
	"""


	def __init__(self, items=None, constants=cpConstants()):
		"""
		Constructor.

		:param items: (optional) Item or list of items in [{<item1>}, {<item2}, ...] format.
		:param contants: (optional) cpConstants instance.
						 By default a new one is created.
		:return: cpObjects instance.
		"""

		cpData.__init__(self, constants)
		if items:
			self.add(items)


	def __contains__(self, item):
		"""
		Overloading 'in' statement.
		Compared parameters are taken from the cpConstants instance. Object names are case insensitive.

		:param item: Item dict. If empty, an exception is raised.
		:return: True/False.
		"""

		# Item can't be empty...
		if item:

			# Look for an object with the same name (case insensitive).
			# WARNING. If there are multiple object with the same name (but different case) a random one will be chosen (list in the list).
			chk_name = item['body']['name']
			for name in self.objects():
				if name.casefold() == item['body']['name'].casefold():
					chk_name = name

			# Return True/False.
			return cpData.compare(item['body'], self.object(chk_name).get('body', {}), self.constants.cmp_params(item['type']))

		# ... otherwise raise an exception.
		else:
			raise cpException('Input item is empty.')


	def add(self, items, override=False):
		"""
		Add items into a cpObjects instance. Raises an exception if an item doesn't have 'name' or 'type' param.
		By default newly added objects with the same name and type as old ones are ingored.

		:param items: Item or list of items in [{<item1>}, {<item2>}, ...] format.
		:param override: (optional) Override already existing item with the same name and type (True/False).
						 Default value: False.
		:return: List of conflicting items (with the same name and type).
		"""

		if not isinstance(items, list):
			items = [items]

		conflicts = []		# List of conflicting items (same name and type).

		# Process each item.
		for item in items:

			# Item has 'name' param defined.
			if 'name' in item:

				name = item['name']

				# There is already object with the same name and type.
				if self.object(name) and self.type(name) == item.get('type'):

					# Override if specified; otherwise ignore.
					if override:
						self.data.remove(self.object(name))
						cpData.add(self, item)
						if 'uid' in item:
							self.uids[item['uid']] = name

					# Add name to the conflict list.
					conflicts.append(name)

				# No match - only add new item.
				else:
					cpData.add(self, item)
					if 'uid' in item:
						self.uids[item['uid']] = name

			# Item doesn't have 'name' - raise an exception.
			else:
				raise cpException('Item must have a name:\n' + repr(item))

		return conflicts


	def object(self, id):
		"""
		Return object dict. based on ID (name/UID).
		If id is not found an empty dict. is returned.

		:param id: Name/uid.
		:return: Object dictionary or empty dict.
		"""

		for item in self.data:
			if item['body']['name'] == id or item['body'].get('uid') == id:
				return item
		return {}


	def objects(self, types=None):
		"""
		Return list of object names based on a type.
		If there are not objects of specific type, an empty list is returned instead.

		:param types: (optional) Object type.
					  If not specified all names are returned.
		:return: List of object names.
		"""

		if types:
			return self.search(attr_dict={'type': types})
		else:
			return self.search()


	def common(self, cpobj_inst, types=None):
		"""
		Return list of common objects within two cpObjects instances.
		External objects are compared against 'self'.

		:param cpobj_inst: External cpObjects instance.
		:param types: (optional) List of specific types.
					  If not defined, all types are checked.
		:return: List of object names.
		"""

		common_list = []
		for name in cpobj_inst.objects(types):
			if cpobj_inst.object(name) in self:
				common_list.append(name)
		return common_list


	def twins(self, item):
		"""
		Return list of all objects with the same configuration (may have different names).

		:param item: Item dictionary.
		:return: List of object names.
		"""

		if item:
			twins_list = []
			for name in self.objects(item['type']):
				if cpData.compare(item['body'], self.object(name)['body'], self.constants.cmp_params(item['type'])):
					twins_list.append(name)
			return twins_list
		else:
			raise cpException('Input item is empty.')


	def search(self, search_list=[], attr_dict={}):
		"""
		Search for items that meet specific criteria.
		First items are filtered by their attributes, then by body parameters.
		If no arguments are used, simply all items are returned.

		:param search_list: (optional) Search list in [{<param/nested-param-tuple>: <value>/[<values_list>]}, {...}] format.
							Example: [{'name': ['host1', 'host2'], 'color': 'red', ('nat', 'enabled'): 'yes'}].
							Between statements (dictionaries) in a list there is logical OR.
							Within a single statement (dictionary), between values in a list there is logical OR.
							Within a single statement (dictionary), between dictionary keys there is logical AND.
		:praram attr_dict: (optional) Dictionary in {<attribute>: <value>/[values_list>]} format.
						   Between values in a list there is logical OR.
						   Between dictionary keys there is logical AND.
		:return: List of object names.
		"""

		return [item['body']['name'] for item in cpData.search(self, search_list, attr_dict)]


	def replace(self, id, replace_list):
		"""
		Change a specific object.

		:param id: Name or UID of an object.
		:param replace_list: Replace list in [{'key': <param/nested-param-tuple>, 'val_before': <value/None>, 'val_after': <value/None>}, {...}] format.
							 Example: [{'key': 'color', 'val_before': 'blue', 'val_after': 'red'}, {'key': ('nat', 'enabled'), 'val_before': 'yes', 'val_after': 'no'}].
							 If 'val_before' is 'None', a new value will be appended (list) or it will replace the current value (string or int).
							 If 'val_after' is 'None', a value will be removed (list) or set to 'None' (string or int).
		:return: A list of boolean values indicating that specific field defined in the 'replace_list' was replaced.
		"""

		return cpData.replace(self.object(id)['body'], replace_list)


	def crop(self, id, param_list=[], skip_none=True):
		"""
		Crop body content of an object based on the list of required parameters.
		If object is not found (None, {}) tuple is returned.

		:param id: Name/UID.
		:param param_list: (optional) List of (nested) parameters that need to be displayed, e.g. ['name', ('nat', 'enabled')].
						   If not specified, default parameters from cpConstants are used.
						   Non-existing parameters are ignored.
		:param skip_none: (optional) True/False. Skip params with empty values.
						  Default value: True.
		:return: (<type>, <body>) tuple.
		"""

		obj = self.object(id)
		if obj:
			if param_list:
				return (obj['type'], cpData.crop(obj['body'], param_list, skip_none))
			else:
				#return cpData.crop(obj['body'], self.constants.types_def[obj['type']]['params'], skip_none)
				return (obj['type'], cpData.crop(obj['body'], self.constants.params(obj['type']), skip_none))
		else:
			return (None, {})


	def references(self, ids, include_nested=False):
		"""
		Return a list of object names/UIDs referenced in selected objects.

		:param ids: List of IDs (names or UIDs).
		:param include_nested: (optional) True/False - include nested references (e.g. members of groups within other groups).
							   Default value: False.
		:return: List of IDs of referenced objects (not merged with the original list of IDs).
		"""

		ref_set = set()
		ref_types = list({type for type_list in self.constants.ref_dict.values() for type in type_list})		# List of types containing references.
		stack = ids		# Object stack (initially list of original objects).

		# Proceed while 'stack' contains any IDs.
		while stack:

			nested_body = NestedDict(self.object(stack.pop(0))['body'])

			# Process params that contain references.
			for param in self.constants.ref_dict:

				# Param within object.
				if param in nested_body:

					# Value is a list.
					if isinstance(nested_body[param], list):

						# Add referenced objects.
						ref_set.update(nested_body[param])

						# Include nested objects - add objects to the stack that also contain references.
						if include_nested:
							for id in nested_body[param]:
								if self.type(id) in ref_types:
									stack.append(id)

					# Value is string or int.
					else:

						# Add referenced objects.
						ref_set.add(nested_body[param])

						# Include nested objects - add objects to the stack that also contain references.
						if include_nested:
							if self.type(nested_body[param]) in ref_types:
								stack.append(nested_body[param])

		return list(ref_set)


	def type(self, id):
		"""
		Return object type based on id (name/UID).
		If object not found return 'None'.

		:param id: Name/uid.
		:return: Object type or 'None'.
		"""

		item = self.object(id)
		if item:
			return item['type']
		else:
			return None


	def types(self):
		"""
		Returns all types of objects.

		:return: List of types.
		"""

		return list({item['type'] for item in self.data})
