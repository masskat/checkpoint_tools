import json
from ndict import NestedDict
from .cptools_exceptions import cpException

class cpConstants():
    """
    Constants used to extract and process Check Point data.
    """

    def __init__(self):

        # Data types.
        self.types_def = {

            # Each type has following options:
            #  - 'ver': API version
            #  - 'cat': Category - network, service, layer, access-rulebase, nat-rulebase
            #  - 'params': Default params used by crop methosd.

            # Network
        	'host': {
        		'ver': 1.1,
        		'cat': 'network',
                'cmp': [
                    'ipv4-address',
                    'ipv6-address',
                    ('nat-settings', 'auto-rule'),
                    ('nat-settings', 'ipv4-address'),
                    ('nat-settings', 'ipv6-address'),
                    ('nat-settings', 'hide-behind'),
                    ('nat-settings', 'install-on'),
                    ('nat-settings', 'method')
                ],
                'params': [
                    'name',
                    'ipv4-address',
                    'ipv6-address',
                    ('nat-settings', 'auto-rule'),
                    ('nat-settings', 'ipv4-address'),
                    ('nat-settings', 'ipv6-address'),
                    ('nat-settings', 'hide-behind'),
                    ('nat-settings', 'install-on'),
                    ('nat-settings', 'method'),
                    'comments',
                    'color'
                ]
        	},
        	'network': {
        		'ver': 1.1,
        		'cat': 'network',
                'cmp': [
                    'subnet4',
                    'subnet6',
                    'subnet-mask',
                    'mask-length6',
                    ('nat-settings', 'auto-rule'),
                    ('nat-settings', 'ipv4-address'),
                    ('nat-settings', 'ipv6-address'),
                    ('nat-settings', 'hide-behind'),
                    ('nat-settings', 'install-on'),
                    ('nat-settings', 'method')
                ],
                'params': [
                    'name',
                    'subnet4',
                    'subnet6',
                    'subnet-mask',
                    'mask-length6',
                    ('nat-settings', 'auto-rule'),
                    ('nat-settings', 'ipv4-address'),
                    ('nat-settings', 'ipv6-address'),
                    ('nat-settings', 'hide-behind'),
                    ('nat-settings', 'install-on'),
                    ('nat-settings', 'method'),
                    'comments',
                    'color'
                ]
        	},
        	'address-range': {
        		'ver': 1.1,
        		'cat': 'network',
                'cmp': [
                    'ipv4-address-first',
                    'ipv4-address-last',
                    ('nat-settings', 'auto-rule'),
                    ('nat-settings', 'ipv4-address'),
                    ('nat-settings', 'ipv6-address'),
                    ('nat-settings', 'hide-behind'),
                    ('nat-settings', 'install-on'),
                    ('nat-settings', 'method')
                ],
                'params': [
                    'name',
                    'ipv4-address-first',
                    'ipv4-address-last',
                    ('nat-settings', 'auto-rule'),
                    ('nat-settings', 'ipv4-address'),
                    ('nat-settings', 'ipv6-address'),
                    ('nat-settings', 'hide-behind'),
                    ('nat-settings', 'install-on'),
                    ('nat-settings', 'method'),
                    'comments',
                    'color'
                ]
        	},
        	'group': {
        		'ver': 1.1,
        		'cat': 'network',
                'cmp': [
                    'members'
                ],
                'params': [
                    'name',
                    'members',
                    'comments',
                    'color'
                ]
        	},
        	'group-with-exclusion': {
        		'ver': 1.1,
        		'cat': 'network',
                'cmp': [
                    'except',
                    'include'
                ],
                'params': [
                    'name',
                    'except',
                    'include',
                    'comments',
                    'color'
                ]
        	},

            # Services
        	'service-tcp': {
        		'ver': 1.1,
        		'cat': 'service',
                'cmp': [
                    'port',
                    'match-by-protocol-signature',
                    'override-default-settings',
                    'session-timeout',
                    'use-default-session-timeout',
                    'match-for-any',
                    'sync-connections-on-cluster',
                    ('aggressive-aging', 'enable'),
                    ('aggressive-aging', 'timeout'),
                    ('aggressive-aging', 'use-default-timeout'),
                    ('aggressive-aging', 'default-timeout'),
                    'keep-connections-open-after-policy-installation'
                ],
                'params': [
                    'name',
                    'port',
                    'match-by-protocol-signature',
                    'override-default-settings',
                    'session-timeout',
                    'use-default-session-timeout',
                    'match-for-any',
                    'sync-connections-on-cluster',
                    ('aggressive-aging', 'enable'),
                    ('aggressive-aging', 'timeout'),
                    ('aggressive-aging', 'use-default-timeout'),
                    ('aggressive-aging', 'default-timeout'),
                    'keep-connections-open-after-policy-installation',
                    'comments',
                    'color'
                ]
        	},
        	'service-udp': {
        		'ver': 1.1,
        		'cat': 'service',
                'cmp': [
                    'accept-replies',
                    'port',
                    'match-by-protocol-signature',
                    'override-default-settings',
                    'session-timeout',
                    'use-default-session-timeout',
                    'match-for-any',
                    'sync-connections-on-cluster',
                    ('aggressive-aging', 'enable'),
                    ('aggressive-aging', 'timeout'),
                    ('aggressive-aging', 'use-default-timeout'),
                    ('aggressive-aging', 'default-timeout'),
                    'keep-connections-open-after-policy-installation'
                ],
                'params': [
                    'name',
                    'accept-replies',
                    'port',
                    'match-by-protocol-signature',
                    'override-default-settings',
                    'session-timeout',
                    'use-default-session-timeout',
                    'match-for-any',
                    'sync-connections-on-cluster',
                    ('aggressive-aging', 'enable'),
                    ('aggressive-aging', 'timeout'),
                    ('aggressive-aging', 'use-default-timeout'),
                    ('aggressive-aging', 'default-timeout'),
                    'keep-connections-open-after-policy-installation',
                    'comments',
                    'color'
                ]
        	},
        	'service-icmp': {
        		'ver': 1.1,
        		'cat': 'service',
                'cmp': [
                    'icmp-code',
                    'icmp-type',
                    'keep-connections-open-after-policy-installation'
                ],
                'params': [
                    'name',
                    'icmp-code',
                    'icmp-type',
                    'keep-connections-open-after-policy-installation',
                    'comments',
                    'color'
                ]
        	},
        	'service-icmp6': {
        		'ver': 1.1,
        		'cat': 'service',
                'cmp': [
                    'icmp-code',
                    'icmp-type',
                    'keep-connections-open-after-policy-installation'
                ],
                'params': [
                    'name',
                    'icmp-code',
                    'icmp-type',
                    'keep-connections-open-after-policy-installation',
                    'comments',
                    'color'
                ]
        	},
        	'service-sctp': {
                'ver': 1.1,
        		'cat': 'service',
                'cmp': [
                    'port',
                    'override-default-settings',
                    'session-timeout',
                    'use-default-session-timeout',
                    'match-for-any',
                    'sync-connections-on-cluster',
                    ('aggressive-aging', 'enable'),
                    ('aggressive-aging', 'timeout'),
                    ('aggressive-aging', 'use-default-timeout'),
                    ('aggressive-aging', 'default-timeout'),
                    'keep-connections-open-after-policy-installation'
                ],
                'params': [
                    'name',
                    'port',
                    'override-default-settings',
                    'session-timeout',
                    'use-default-session-timeout',
                    'match-for-any',
                    'sync-connections-on-cluster',
                    ('aggressive-aging', 'enable'),
                    ('aggressive-aging', 'timeout'),
                    ('aggressive-aging', 'use-default-timeout'),
                    ('aggressive-aging', 'default-timeout'),
                    'keep-connections-open-after-policy-installation',
                    'comments',
                    'color'
                ]
        	},
        	'service-other': {
        		'ver': 1.1,
        		'cat': 'service',
                'cmp': [
                    'accept-replies',
                    'ip-protocol',
                    'override-default-settings',
                    'session-timeout',
                    'use-default-session-timeout',
                    'match-for-any',
                    'sync-connections-on-cluster',
                    ('aggressive-aging', 'enable'),
                    ('aggressive-aging', 'timeout'),
                    ('aggressive-aging', 'use-default-timeout'),
                    ('aggressive-aging', 'default-timeout'),
                    'keep-connections-open-after-policy-installation'
                ],
                'params': [
                    'name',
                    'accept-replies',
                    'ip-protocol',
                    'override-default-settings',
                    'session-timeout',
                    'use-default-session-timeout',
                    'match-for-any',
                    'sync-connections-on-cluster',
                    ('aggressive-aging', 'enable'),
                    ('aggressive-aging', 'timeout'),
                    ('aggressive-aging', 'use-default-timeout'),
                    ('aggressive-aging', 'default-timeout'),
                    'keep-connections-open-after-policy-installation',
                    'comments',
                    'color'
                ]
        	},
            'service-dce-rpc': {
                'ver': 1.1,
                'cat': 'service',
                'cmp': [
                    'interface-uuid',
                    'keep-connections-open-after-policy-installation'
                ],
                'params': [
                    'name',
                    'interface-uuid',
                    'keep-connections-open-after-policy-installation',
                    'comments',
                    'color'
                ]
            },
            'service-rpc': {
                'ver': 1.1,
                'cat': 'service',
                'cmp': [
                    'program-number',
                    'keep-connections-open-after-policy-installation'
                ],
                'params': [
                    'name',
                    'program-number',
                    'keep-connections-open-after-policy-installation',
                    'comments',
                    'color'
                ]
            },
        	'service-group': {
        		'ver': 1.1,
        		'cat': 'service',
                'cmp': [
                    'members'
                ],
                'params': [
                    'name',
                    'members',
                    'comments',
                    'color'
                ]
        	},

            # Layers
            'access-layer': {
                'ver': 1.1,
                'cat': 'layer',
                'cmp': [],
                'params': [
                    'name',
                    'applications-and-url-filtering',
                    'content-awareness',
                    'detect-using-x-forward-for',
                    'firewall',
                    'mobile-access',
                    'shared'
                ]
            },

            # Access rules
            'access-rule': {
                'ver': 1.1,
                'cat': 'access-rulebase',
                'cmp': [],
                'params': [
                    'layer',
                    'position',
                    'name',
                    'source',
                    'source-negate',
                    'destination',
                    'destination-negate',
                    'service',
                    'service-negate',
                    'vpn',
                    'action',
                    'action-settings',
                    'inline-layer',
                    ('track', 'type'),
                    ('track', 'per-session'),
                    ('track', 'per-connection'),
                    ('track', 'accounting'),
                    ('track', 'enable-firewall-session'),
                    ('track', 'alert'),
                    'content',
                    'content-negate',
                    'content-direction',
                    'time',
                    'comments',
                    'enabled',
                    'install-on'
                ]
            },
            'access-section': {
                'ver': 1.1,
                'cat': 'access-rulebase',
                'cmp': [],
                'params': [
                    'layer',
                    'position',
                    'name'
                ]
            },

            # NAT rules
            'nat-rule': {
                'ver': 1.1,
                'cat': 'nat-rulebase',
                'cmp': [],
                'params': [
                    'package',
                    'position',
                    'name',
                    'method',
                    'original-source',
                    'translated-source',
                    'original-destination',
                    'translated-destination',
                    'original-service',
                    'translated-service',
                    'comments',
                    'enabled',
                    'install-on'
                ]
            },
            'nat-section': {
                'ver': 1.1,
                'cat': 'nat-rulebase',
                'cmp': [],
                'params': [
                    'package',
                    'position',
                    'name'
                ]
            }

        }

        # Params with references.
        self.ref_dict = {
    		('track', 'type'): ['access-rule'],
    		'source': ['access-rule'],
    		'destination': ['access-rule'],
    		'service': ['access-rule'],
    		'vpn': ['access-rule'],
    		'action': ['access-rule'],
    		'content': ['access-rule'],
    		'time': ['access-rule'],
    		'install-on': ['access-rule', 'nat-rule'],
    		'inline-layer': ['access-rule'],
            'members': ['group', 'service-group'],
            'original-source': ['nat-rule'],
            'translated-source': ['nat-rule'],
            'original-destination': ['nat-rule'],
            'translated-destination': ['nat-rule'],
            'original-service': ['nat-rule'],
            'translated-service': ['nat-rule']
        }

        # Built-in objects.
        self.builtin_obj = {
            'Policy Targets': 'Global',
            'Log': 'Track',
            'None': 'Track',
            'Inner Layer': 'Global',
            'Original': 'Global',
            'Accept': 'RulebaseAction',
            'Drop': 'RulebaseAction',
            'Any': 'CpmiAnyObject',
            'All': 'CpmiAnyObject'
        }

        # Translations between different object names (used by uids2names() method).
        self.translations = {
            'Inner Layer': 'Apply Layer'
        }

        # Dbedit definitions.
        self.regex_dbedit = [
            # Single match dict syntax:
            #  - 'type': <item type>
            #            If type is 'None' specific pattern exists in multiple items and this param is not checked.
            #  - 'regex': <regex matching single group (id) or two groups (id, value)>
            #  - 'keys': {<key1>: {<val_dict1>}, <key2>: {<val_dict2>}}
            #            Value found in <val_dict> is translated to another value. Other values are not changed (except the situation when <val_dict> key is 'None').
            #            If <val_dict> key is 'None' every unmatched value will be translated to a default value.
            #            If result is 'None' the whole key will be omitted.
            #  - 'int': True/False - (Optional) Value should be treated as integer and not as a string (default).
            #  - 'stop': True/False - Stop processing further regex rules. If 'False' multiple matches may be returned. Correct match should be chosen based on type.

            # Host
            {
                'type': 'host',
                'regex': "^create host_plain ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'host',
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) ipaddr ([0-9\.]+)",
                'keys': {'ipv4-address': {}},
                'stop': False   # Network object has the same syntax for matching an IP.
            },

            # Network
            {
                'type': 'network',
                'regex': "^create network ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'network',
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) ipaddr ([0-9\.]+)",
                'keys': {'subnet4': {}},
                'stop': True
            },
            {
                'type': 'network',
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) netmask ([0-9\.]+)",
                'keys': {'subnet-mask': {}},
                'stop': True
            },

            # Address range
            {
                'type': 'address-range',
                'regex': "^create address_range ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'address-range',
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) ipaddr_first ([0-9\.]+)",
                'keys': {'ipv4-address-first': {}},
                'stop': True
            },
            {
                'type': 'address-range',
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) ipaddr_last ([0-9\.]+)",
                'keys': {'ipv4-address-last': {}},
                'stop': True
            },

            # Group
            {
                'type': 'group',
                'regex': "^create network_object_group ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'group',
                'regex': "^addelement network_objects ([a-zA-Z0-9\-\_\.]+) '' network_objects:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'members': {}},
                'stop': True
            },

            # Group with exclusion
            {
                'type': 'group-with-exclusion',
                'regex': "^create group_with_exception ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'group-with-exclusion',
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) base network_objects:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'include': {}},
                'stop': True
            },
            {
                'type': 'group-with-exclusion',
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) exception network_objects:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'except': {}},
                'stop': True
            },

            # Common network objects params.
            {
                'type': None,
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) color \"?([a-zA-Z0-9\-\_\. ]+)\"?",
                'keys': {
                    'color': {
                        'aquamarine1': 'aquamarine',
                        'black': 'black',
                        'dodgerblue3': 'blue',
                        'sienna': 'brown',
                        'burlywood4': 'burlywood',
                        'light coral': 'coral',
                        'blue': 'crete blue',
                        'cyan': 'cyan',
                        'blue1': 'dark blue',
                        'gold3': 'dark gold',
                        'gray90': 'dark gray',
                        'dark green': 'dark green',
                        'darkorange3': 'dark orange',
                        'firebrick': 'firebrick',
                        'forest green': 'forest green',
                        'gold3': 'gold',
                        'gray83': 'gray',
                        'dark khaki': 'khaki',
                        'lemonchiffon': 'lemon chiffon',
                        'green': 'light green',
                        'magenta': 'magenta',
                        'navy blue': 'navy blue',
                        'olive drab': 'olive',
                        'orange': 'orange',
                        'dark orchid': 'orchid',
                        'deep pink': 'pink',
                        'medium orchid': 'purple',
                        'red': 'red',
                        'darkseagreen3': 'sea green',
                        'deepskyblue1': 'sky blue',
                        'medium slate blue': 'slate blue',
                        'lightseagreen': 'turquoise',
                        'medium violet red': 'violet red',
                        'yellow': 'yellow',
                        'lightskyblue4': 'turquoise',
                        None: 'black'
                    }
                },
                'stop': True
            },
            {
                'type': None,
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) comments \"?([a-zA-Z0-9#\-\<\>\(\)\+\/\_\. ]+)\"?",
                'keys': {'comments': {}},
                'stop': True
            },
            {
                'type': None,
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) add_adtr_rule (true|false)",
                'keys': {('nat-settings', 'auto-rule'): {'true': True, 'false': False}},
                'stop': True
            },
            {
                'type': None,
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) NAT:netobj_adtr_method (adtr_hide|adtr_static)",
                'keys': {('nat-settings', 'method'): {'adtr_hide': 'hide', 'adtr_static': 'static'}},
                'stop': True
            },
            {
                'type': None,
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) NAT:valid_ipaddr ([0-9\.]+)",
                'keys': {('nat-settings', 'ipv4-address'): {'0.0.0.0': None}, ('nat-settings', 'hide-behind'): {'0.0.0.0': 'gateway', None: 'ip-address'}},
                'stop': True
            },
            {
                'type': None,
                'regex': "^modify network_objects ([a-zA-Z0-9\-\_\.]+) NAT:the_firewalling_obj [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {('nat-settings', 'install-on'): {}},
                'stop': True
            },

            # TCP service
            {
                'type': 'service-tcp',
                'regex': "^create tcp_service ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },

            # UDP service
            {
                'type': 'service-udp',
                'regex': "^create udp_service ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },

            # ICMP service
            {
                'type': 'service-icmp',
                'regex': "^create icmp_service ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'service-icmp',
                'regex': "^modify services ([a-zA-Z0-9\-\_\.]+) icmp_type ([0-9]+)",
                'keys': {'icmp-type': {}},
                'int': True,
                'stop': True
            },
            {
                'type': 'service-icmp',
                'regex': "^modify services ([a-zA-Z0-9\-\_\.]+) icmp_code ([0-9]+)",
                'keys': {'icmp-code': {}},
                'int': True,
                'stop': True
            },

            # DCE RPC service
            {
                'type': 'service-dce-rpc',
                'regex': "^create dcerpc_service ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'service-dce-rpc',
                'regex': "^modify services ([a-zA-Z0-9\-\_\.]+) uuid ([a-zA-Z0-9\-]+)",
                'keys': {'interface-uuid': {}},
                'stop': True
            },

            # Other service
            {
                'type': 'service-other',
                'regex': "^create other_service ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'service-other',
                'regex': "^modify services ([a-zA-Z0-9\-\_\.]+) protocol ([0-9]+)",
                'keys': {'ip-protocol': {}},
                'int': True,
                'stop': True
            },

            # Service group
            {
                'type': 'service-group',
                'regex': "^create service_group ([a-zA-Z0-9\-\_\.]+)",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'service-group',
                'regex': "^addelement services ([a-zA-Z0-9\-\_\.]+) '' services:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'members': {}},
                'stop': True
            },

            # Common service objects params.
            {
                'type': None,
                'regex': "^modify services ([a-zA-Z0-9\-\_\.]+) color \"?([a-zA-Z0-9\-\_\. ]+)\"?",
                'keys': {
                    'color': {
                        'aquamarine1': 'aquamarine',
                        'black': 'black',
                        'dodgerblue3': 'blue',
                        'sienna': 'brown',
                        'burlywood4': 'burlywood',
                        'light coral': 'coral',
                        'blue': 'crete blue',
                        'cyan': 'cyan',
                        'blue1': 'dark blue',
                        'gold3': 'dark gold',
                        'gray90': 'dark gray',
                        'dark green': 'dark green',
                        'darkorange3': 'dark orange',
                        'firebrick': 'firebrick',
                        'forest green': 'forest green',
                        'gold3': 'gold',
                        'gray83': 'gray',
                        'dark khaki': 'khaki',
                        'lemonchiffon': 'lemon chiffon',
                        'green': 'light green',
                        'magenta': 'magenta',
                        'navy blue': 'navy blue',
                        'olive drab': 'olive',
                        'orange': 'orange',
                        'dark orchid': 'orchid',
                        'deep pink': 'pink',
                        'medium orchid': 'purple',
                        'red': 'red',
                        'darkseagreen3': 'sea green',
                        'deepskyblue1': 'sky blue',
                        'medium slate blue': 'slate blue',
                        'lightseagreen': 'turquoise',
                        'medium violet red': 'violet red',
                        'yellow': 'yellow',
                        'lightskyblue4': 'turquoise',
                        None: 'black'
                    }
                },
                'stop': True
            },
            {
                'type': None,
                'regex': "^modify services ([a-zA-Z0-9\-\_\.]+) port \"?([0-9\-\>\<]+)\"?",
                'keys': {'port': {}},
                'stop': True
            },
            {
                'type': None,
                'regex': "^modify services ([a-zA-Z0-9\-\_\.]+) timeout ([0-9]+)",
                'keys': {'session-timeout': {}},
                'int': True,
                'stop': True
            },
            {
                'type': None,
                'regex': "^modify services ([a-zA-Z0-9\-\_\.]+) comments \"?([a-zA-Z0-9#\-\<\>\(\)\+\/\_\. ]+)\"?",
                'keys': {'comments': {}},
                'stop': True
            },

            # Access section
            {
                'type': 'access-section',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):header_text \"?([a-zA-Z0-9#\-\<\>\(\)\+\/\_\. ]+)\"?",
                'keys': {'name': {}},
                'stop': True
            },

            # Access rule
            {
                'type': 'access-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):name \"?([a-zA-Z0-9#\-\<\>\(\)\+\/\_\. ]+)\"?",
                'keys': {'name': {}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):disabled (true|false)",
                'keys': {'enabled': {'true': False, 'false': True}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):src:'' [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'source': {}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):src:op '(|not in)'",
                'keys': {'source-negate': {'not in': True, '': False}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):dst:'' [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'destination': {}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):dst:op '(|not in)'",
                'keys': {'destination-negate': {'not in': True, '': False}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):services:'' [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'service': {}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):src:op '(|not in)'",
                'keys': {'service-negate': {'not in': True, '': False}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):through:'' [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'vpn': {}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):time: [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'time': {}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):action [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'action': {'accept': 'Accept', 'drop': 'Drop'}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):track tracks:([a-zA-Z0-9\-\_\.]+)",
                'keys': {('track', 'type'): {}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):install:'' [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'install-on': {'Any': 'Policy Targets'}},
                'stop': True
            },
            {
                'type': 'access-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule:([0-9]+):comments \"?([a-zA-Z0-9#\-\<\>\(\)\+\/\_\. ]+)\"?",
                'keys': {'comments': {}},
                'stop': True
            },

            # Access section
            {
                'type': 'nat-section',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):header_text \"?([a-zA-Z0-9#\-\<\>\(\)\+\/\_\. ]+)\"?",
                'keys': {'name': {}},
                'stop': True
            },

            # NAT rule
            {
                'type': 'nat-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):disabled (true|false)",
                'keys': {'enabled': {'true': False, 'false': True}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):src_adtr [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'original-source': {}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):dst_adtr [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'original-destination': {}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):services_adtr [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'original-service': {}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):src_adtr_translated:'' [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'translated-source': {'Any': 'Original'}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):dst_adtr_translated:'' [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'translated-destination': {'Any': 'Original'}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):services_adtr_translated:'' [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'translated-service': {'Any': 'Original'}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):src_adtr_translated (translate_static|translate_hide)",
                'keys': {'method': {'translate_static': 'static', 'translate_hide': 'hide'}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^addelement fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):install [a-z_]+:([a-zA-Z0-9\-\_\.]+)",
                'keys': {'install-on': {'Any': 'Policy Targets'}},
                'stop': True
            },
            {
                'type': 'nat-rule',
                'regex': "^modify fw_policies ##[a-zA-Z0-9]+ rule_adtr:([0-9]+):comments \"?([a-zA-Z0-9#\-\<\>\(\)\+\/\_\. ]+)\"?",
                'keys': {'comments': {}},
                'stop': True
            }

        ]


    def types(self, cats=[], ver=1.1):
    	"""
    	Return a list of object types for specified categories.

    	:param cats: (optional) Single category or list of categories.
    				 If not specied all commands are returned.
        :param ver: (optional) API version (1.1 by default).
    	:return: List of types for specified categories.
    	"""

    	if isinstance(cats, str):
    		cats = [cats]
    	if cats:
    		types_out = []
    		for cat in cats:
    			types_out += [type for type in self.types_def if self.types_def[type]['ver'] <= ver and self.types_def[type]['cat'] == cat]
    	else:
    		types_out = list(self.types_def.keys())
    	return types_out


    def params(self, type):
        """
        Return default params for specified type.
        Raise an exceptions if type is not found.

        :param type: Item type.
        :return: List of nested params.
        """

        if type in self.types_def:
            return self.types_def[type]['params']
        else:
            raise cpException('Type not found: ' + type)


    def cmp_params(self, type):
        """
        Return compare params for specified type.
        Raise an exceptions if type is not found.

        :param type: Item type.
        :return: List of nested params.
        """

        if type in self.types_def:
            return self.types_def[type]['cmp']
        else:
            raise cpException('Type not found: ' + type)
