
def api_publish(cpwebapi, success):
    """
    Publish or discard changes.

    :param cpwebapi: cpWebAPI instance.
    :param success: Status of previous API operations: True (publish changes), False (discard changes).
    :return: Publish/discard operation status (True/False).
    """

    if success:
        print('\nPublishing changes...', end=' ')
        res = cpwebapi.publish()
        if res.success:
            print('Done\n\n')
        else:
            print('Failed:\n' + res.error_message + '\n')
    else:
        print('\nDiscarding changes...', end=' ')
        res = cpwebapi.discard()
        if res.success:
            print('Done\n\n')
        else:
            print('Failed:\n' + res.error_message + '\n')

    return res.success


def api_add_objects(cpwebapi, cpobjects, obj_list, ignore_warnings=True):
    """
    Add objects via API.

    :param cpwebapi: cpWebAPI instance.
    :param cpobjects: cpObjects instance.
    :param obj_list: List of object names.
    :param ignore_warnings: (optional) Ignore warnings during API calls (e.g. about duplicated IP).
                            Default value: True.
    :return: Operation status (True/False)
    """

    grp_params = ['name', 'comments', 'color']
    grp_types = ['group', 'service-group']
    success = True

    # Moving groups with exclusions at the end.
    for obj_name in obj_list:
        if cpobjects.type(obj_name) == 'group-with-exclusion':
            obj_list.remove(obj_name)
            obj_list.append(obj_name)

    # Add objects (except exclusion groups and group members)
    for obj_name in obj_list:

        # Post changes.
        if cpobjects.type(obj_name) in grp_types:
            res = cpwebapi.post(*cpobjects.crop(obj_name, grp_params), 'add', {'ignore-warnings': ignore_warnings})
        else:
            res = cpwebapi.post(*cpobjects.crop(obj_name), 'add', {'ignore-warnings': ignore_warnings})

        # Check API call status.
        if res.success:
            print('Added \'' + obj_name + '\' ' + cpobjects.type(obj_name))
        else:
            print('Failed to add \'' + obj_name + '\' ' + cpobjects.type(obj_name) + ':\n' + res.error_message)
            success = False

    # Set group members.
    for obj_name in obj_list:

        # Process only groups.
        if cpobjects.type(obj_name) in grp_types:

            # Post changes.
            res = cpwebapi.post(*cpobjects.crop(obj_name, ['name', 'members']), 'set', {'ignore-warnings': ignore_warnings})

            # Check API call status.
            if res.success:
                print('Added members to \'' + obj_name + '\' ' + cpobjects.type(obj_name))
            else:
                print('Failed to add members to \'' + obj_name + '\' ' + cpobjects.type(obj_name) + ':\n' + res.error_message)
                success = False

    api_publish(cpwebapi, success)

    return success


def api_add_rules(cpwebapi, container, cprules, positions=[], include_sections=True, args={}):
    """
    Add access/nat rules via API.

    :param cpwebapi: cpWebAPI instance.
    :param container: Layer (access rules), Package (nat rules).
    :param cprules: cpRulebase instance.
	:param positions: (optional) List of positions (single int. - single rule; tuple with two ints. - range of rules.).
					  If no positions are specified, all rules from a container are added.
	:param include_sections: (optional) Include section titles if associated with rules (True by default).
	:param args: (optional) Additional ad-hoc arguments that will be added to the body before API call (e.g. rule position).
    :return: Operation status (True/False)
    """

    success = True

    for (type, body, position) in cprules.gen_crop(container, positions, include_sections=include_sections):
        id_str = str(position) if type in ['access-rule', 'nat-rule'] else '\'' + body['name'] + '\''
        res = cpwebapi.post(type, body, 'add', args)
        if res.success:
            print('Added ' + type + ' ' + id_str)
        else:
            print('Failed to add ' + type + ' ' + id_str + ':\n' + res.error_message + '\n')
            success = False

    api_publish(cpwebapi, success)

    return success


def merge_objects(cpobj_new, cpobj_old, prefix='new_'):
    """
    Merge object from two cpObjects instances. Only unique objects are added. The same objects from two instances are omitted.
    Function returns a list of unique object names from the 'cpobj_new' cpObjects instance that do not belong to the 'cpobj_old' instance.
    If a conflict is detected (two objects with the same name but different configs, the new object is renamed using 'prefix' param.).

    :param cpobj_new: cpObjects instance representing new objects to be added.
    :param cpobj_old: cpObjects instance representing old objects already existing.
    :param prefic: (optional) Name prefix add to a conflicting object.
    :return: (<new-objects-to-be-added>, <renamed-conflicting-objects>) tuple.
    """

    new_obj_list = cpobj_new.objects()  # Names of new objects.
    old_obj_list = cpobj_old.objects()  # Names of old objects.
    obj_list = []                       # List of unique objects to be added.
    neqobj_set = set()                  # Names of not-equal objects to be modified/renamed (functions as stack in while loop).
    chngobj_set = set()                 # Names of changed objects (before renaming).

    # Process all new object names.
    for obj_name in new_obj_list:

        # Name is in the old object list.
        if obj_name.casefold() in [v.casefold() for v in old_obj_list]:

            # Name exists but the object is not the same. Rename the object.
            # Update non-equal and changed objects sets.
            if not cpobj_new.object(obj_name) in cpobj_old:
                neqobj_set.add(obj_name)
                chngobj_set.add(obj_name)

        # Name not in the old object list - update object list.
        else:
            obj_list.append(obj_name)

    # Process non-equal objects until the set is empty.
    while neqobj_set:

        # Random non-equal object name,
        neqobj = neqobj_set.pop()

        # Replacement list.
        replace_list = [
            {'key': 'members', 'val_before': neqobj, 'val_after': prefix + neqobj},
            {'key': 'except', 'val_before': neqobj, 'val_after': prefix + neqobj},
            {'key': 'include', 'val_before': neqobj, 'val_after': prefix + neqobj}
        ]

        # Modify all objects containing current non-equal object.
        # Any object modified this way is also added to the non-equal objects list if it is not new (not in the 'obj_list').
        for obj_name in new_obj_list:
            if any(cpobj_new.replace(obj_name, replace_list)) and obj_name not in obj_list:
                neqobj_set.add(obj_name)
                chngobj_set.add(obj_name)

    # Rename changed objects.
    for obj_name in chngobj_set:
        cpobj_new.object(obj_name)['body']['name'] = prefix + obj_name

    # Return (<objects-to-be-added>, <renamed-conflicting-objects>) tuple.
    return (obj_list, [prefix + obj_name for obj_name in chngobj_set])
