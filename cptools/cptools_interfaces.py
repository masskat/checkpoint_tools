import json
import csv
import re
from ndict import NestedDict
from .cptools_exceptions import cpException
from .cptools_constants import cpConstants



class cpWebAPI():
	"""
	Check Point web API interface

	Instance attributes:
	  - 'apiclient': APIClient instance.
	  - 'apidata': Keeps data obtained via API in format {'container_key': [<data>]}. Changed after each 'get' API call.
	  - 'apiresponse': APIResponse instance from the last 'get' API call.
	"""

	def __init__(self, apiclient):
		"""
		Constructor.
		"""

		self.apiclient = apiclient
		self.apidata = {}
		self.apiresponse = {}


	def __str__(self):
		"""
		Print instance ('apidata') in human redable format.
		"""
		return json.dumps(self.apidata, indent=4)


	def __repr__(self):
		"""
		Reproduce instance ('apidata').
		"""
		return repr(self.apidata)


	def get(self, api_query):
		"""
		Get items from SMS server via API. Data is saved in the 'apidata' attribute.
		On successful download information is saved in 'apidata'. On failure - 'apidata' = {}.
		Original APIResonse object is saved in the 'apiresponse' attribute.

		:param api_query: APIClient 'api_query' function.
		:return: Operation result (True/False).
		"""

		self.apiresponse = api_query
		if self.apiresponse.success:
			self.apidata = self.apiresponse.data
		else:
			self.apidata = {}
		return self.apiresponse.success


	def gen_get(self, gen_api_query):
		"""
		Generator function. Get items from SMS server via API. Data is saved in the 'apidata' attribute.
		Yields '<current>/<total>' string showing progress of the download process or 'False' on failure.
		On successful download of a data chunk partial information is still saved in 'apidata'. On failure - 'apidata' = {}.
		Original APIResonse object is saved in the 'apiresponse' attribute for each iteration.

		:param gen_api_query: APIClient 'gen_api_query' generator function.
		:yield: On successful download of a data chunk - '<current>/<total>' string showing progress.
				On failure - False.
		"""

		for api_res in gen_api_query:
			self.apiresponse = api_res
			if api_res.success:
				self.apidata = api_res.data
				if api_res.data['total'] == 0:
					yield '0/0'
				else:
					yield str(api_res.data['to']) + '/' + str(api_res.data['total'])
			else:
				self.apidata = {}
				yield False


	def gen_get_rulebase(self, cat, container, details_level='standard'):
		"""
		Generator function. Download a rulebase.

		:param cat: Rulebase category (access-rulebase/nat-rulebase).
		:param container: Layer or package depending on rulebase type.
		:param details_level: (optional) Details level: uid/standard/full
							  By default 'standard' level is used.
		:return: 'gen_get' method.
		"""

		containter_type = 'package' if cat == 'nat-rulebase' else 'name'
		return self.gen_get(self.apiclient.gen_api_query('show-' + cat, details_level=details_level, container_keys=['objects-dictionary', 'rulebase'], payload={containter_type: container, 'use-object-dictionary': True}))


	def gen_get_objects(self, type='object', filter='', ip_only=False, details_level='standard'):
		"""
		Generator function. Download objects.

		:param type: (optional) Object type.
					 Default: 'object' (all objects).
		:param filter: (optional) Search expression (the same syntax as in Smart Console).
		:param ip_only: (optional) True or False. Search by IP address only (used together with the 'filter' param).
						Default: False
		:param details_level: (optional) Details level: uid/standard/full
							  By default 'standard' level is used.
		:return: 'gen_get' method.
		"""

		payload = {'type': type, 'ip-only': False}
		if filter:
			payload['filter'] = filter
		return self.gen_get(self.apiclient.gen_api_query('show-objects', details_level=details_level, payload=payload))


	def get_layers(self, details_level='full'):
		"""
		Download layers.

		:param details_level: (optional) Details level: uid/standard/full
							  By default 'full' level is used.
		:return: Operation result (True/False).
		"""

		return self.get(self.apiclient.api_query('show-access-layers', details_level=details_level))


	def save_rulebase(self, container, cprulebase):
		"""
		Save data obtained from SMS as a cpRulebase object.

		:param container: Layer/package under which the rulebase should be saved.
		:param cprulebase: cpRulebase object.
		:return: List nested layers found in the rulebase (to be processed in next iterations).
		"""

		rules = []			# List of rules in format readable for cpRulebase.
		new_layers = []		# List of new layers.

		# Update uids dictionary of the 'cprulebase' object.
		for obj in self.apidata['objects-dictionary']:
			cprulebase.uids[obj['uid']] = obj['name']

		# Process rules.
		for rule in self.apidata['rulebase']:

			# Rule section - process inner rules.
			if rule['type'] in ['access-section', 'nat-section']:

				# Look for inline layers.
				for inner_rule in rule['rulebase']:
					if 'inline-layer' in inner_rule:
						new_layers.append(cprulebase.uids[inner_rule['inline-layer']])

				# Update rules/new-layers lists.
				rules.append({param: rule[param] for param in rule if param != 'rulebase'})
				rules += rule['rulebase']

			# Ordinary rule.
			else:
				rules.append(rule)
				if 'inline-layer' in rule:
					new_layers.append(cprulebase.uids[rule['inline-layer']])

		# Add rules to the cpRulebase object.
		cprulebase.add(rules, container)

		return new_layers


	def save_objects(self, cpobjects):
		"""
		Save data obtained from SMS as a cpObjects object.

		:param cpobjects: cpObjects object.
		"""

		cpobjects.add(self.apidata['objects'])


	def save_layers(self, cplayers):
		"""
		Save data obtained from SMS as a cpLayers object.

		:param cpobjects: cpLayers object.
		"""

		cplayers.add(self.apidata['access-layers'])


	def post(self, type, body, action, args={}):
		"""
		Add an item via API.

		:param action: Action (add/set/delete).
		:param type: Item type.
		:param body: Item body.
		:param args: (optional) Additional ad-hoc arguments that will be added to the body before API call (e.g. rule position).
		:return: APIResponse object.
		"""

		return self.apiclient.api_call(action + '-' + type, dict(list(body.items()) + list(args.items())))


	def publish(self):
		"""
		Publish changes.

		:return: APIResponse object.
		"""

		return self.apiclient.api_call('publish')


	def discard(self):
		"""
		Discard changes.

		:return: APIResponse object.
		"""

		return self.apiclient.api_call('discard')



class cpCSV():
	"""
	Read/Write CSV files with Check Point configuration.

	CSV file header contains paremeters (in nested-key format) normally used in WAPI. Following formats are used:
	  - Nested keys are belimited by a dot. (e.g. {'nat': {'enabled': ...}} is translated to 'nat.enabled' column).
	  - List values are delimited by a coma (e.g. ['host1', 'host2'] -> 'host1,host2').

	All values read from a CSV file are interpreted as strings except:
	  - True/False which is treated as boolean values.
	  - Numbers which are translated into integers. If you want to treat a number as a string you should include it within single quotes in the CSV file.

	Empty values are ignored. This means that you can mix different types in the same CSV files.
	If specific param. shouldn't be used for specific type value should be empty.

	Besides regular WAPI params additional two are used:
	  - action*: (optional) Action (as defined in API documentation). Can be used together with WAPI functions.
	  - type: Item type (as defined in API documentation, e.g. 'host', 'access-rule', etc.).

	Instance attributes:
	  - 'filename': Name of a CSV file.
	  - 'csvdata': Buffer containing data read from a CSV file or data that can be written to a CSV file.
	  			   Format: [{'action': <action>, 'item': {<item>}}, {<line2>}, ...]
				   Param 'item' in each line should contain dictionary that can be then used by cpData instances ('add()' method).
	"""

	def __init__(self, filename):
		"""
		Constructor.
		"""

		self.filename = filename
		self.csvdata = []


	def __str__(self):
		"""
		Print instance ('csvdata') in human redable format.
		"""
		return json.dumps(self.csvdata, indent=4)


	def __repr__(self):
		"""
		Reproduce instance ('csvdata').
		"""
		return repr(self.csvdata)


	def read(self, delim=';'):
		"""
		Read data from a CSV file.
		Empty value in a CSV file means that specific param should be skipped.

		:param delim: (optional) CSV delimeter (';' by default).
		"""

		# Open file.
		with open(self.filename, newline='') as csv_file:

			# Read CSV file. Each line is mapped to a dict.
			csv_reader = csv.DictReader(csv_file, delimiter=delim)

			line = 2	# CSV line counter.

			# Process CSV lines.
			for row_dict in csv_reader:

				# Dictionary containing information from a single line. First extract action.
				item_dict = {'action': row_dict.pop('action*', None)}

				# Nested dict contaning item data.
				nested_item = NestedDict()

				# Process all keys in a row.
				for key in row_dict:

					# Value is 'None' - something is wrong, raise an expression.
					if row_dict[key] is None:
						raise cpException('Problem detected in line ' + str(line) + ':\n' + repr(row_dict))

					# Continue only if value is not empty string (otherwise ignore param).
					elif row_dict[key] != '':

						# Exctract values.
						val_list = []
						for item in row_dict[key].split(','):
							if item.isdigit():
								val_list.append(int(item))
							elif item.startswith('\'') and item.endswith('\''):
								val_list.append(item.strip('\''))
							elif item == 'True':
								val_list.append(True)
							elif item == 'False':
								val_list.append(False)
							else:
								val_list.append(item)

						# Nested key found.
						if '.' in key:
							nested_item[tuple(key.split('.'))] = val_list[0] if len(val_list) == 1 else val_list

						# Single key.
						else:
							nested_item[key] = val_list[0] if len(val_list) == 1 else val_list

				# Add item data; append data to 'csvdata'; increment line count.
				item_dict['item'] = nested_item.dict
				self.csvdata.append(item_dict)
				line += 1


	def save(self, cpdata):
		"""
		Save local CSV data to a cpData instance or its children instances.

		:param cpdata: cpData instance or its children instances.
		"""

		cpdata.add([row['item'] for row in self.csvdata])


	def newline(self, type, body, action=None, args={}):
		"""
		Add new CSV line to the local buffer.

		:param type: Item type.
		:param body: Item body.
		:param action: (optional) Action (as defined in API documentation).
		:param args: (optional) Additional ad-hoc arguments that will be added to the body before adding to the CSV file (e.g. rule position).
		"""

		self.csvdata.append({'action': action, 'item': dict([('type', type)] + list(body.items()) + list(args.items()))})


	def clear(self):
		"""
		Clear local buffer.
		"""
		self.csvdata = []


	def write(self, delim=';'):
		"""
		Write local CSV data to a file.
		Summary list of headers is calculated based on all lines in the buffer. For non-existing params in specific items an empty value is used.

		:param delim: (optional) CSV delimeter (';' by default).
		"""

		# List of unique nested params used in all items.
		header_list = list({nested_key for row_dict in self.csvdata for nested_key in NestedDict(row_dict['item']).nestedkeys()})

		# Open a file for writting.
		with open(self.filename, 'w') as csv_file:

			# Wrtie CSV header.
			csv_file.write(delim.join(['action*'] + ['.'.join(header) for header in header_list]) + '\n')

			# Process each row (item):
			for row_dict in self.csvdata:

				nested_body = NestedDict(row_dict['item'])

				# List of values to form a list. First set action.
				row_list = [row_dict['action']] if row_dict['action'] else ['']

				# Process each header from the list.
				for header in header_list:

					# Header within body - add value.
					if header in nested_body:

						val = nested_body[header]
						if isinstance(val, list):
							row_list.append(','.join(['\'' + str(item) + '\'' if isinstance(item, str) and item.isdigit() else str(item) for item in val]))
						else:
							if isinstance(val, str) and val.isdigit():
								row_list.append('\'' + str(val) + '\'')
							else:
								# Before adding get rid off all new-lines, tabs, comas and delims.
								row_list.append(str(val).strip().replace('\n', ' ').replace('\t', ' ').replace(',', ' ').replace(delim, ' '))

					# Header not within body - add ''
					else:
						row_list.append('')

				# Write single row to the file.
				csv_file.write(delim.join(row_list) + '\n')



class cpDBedit():
	"""
	Read dbedit files with Check Point configuration.

	Instance attributes:
	  - 'filename': Name of a dbedit file.
	  - 'dbeditdata': Buffer containing data read from a dbedit file.
					  Format: [{'id': <id>, 'item': <NestedDict_instance>, {<entry2>}, ...]
				   	  Param 'item' in each entry should contain dictionary in format that can be then used by cpData instances ('add()' method).
	  - 'constants': cpConstants instance.
	"""

	def __init__(self, filename, constants=cpConstants()):
		"""
		Constructor.

		:param filename: Name of a dbedit file.
		:param contants: (optional) cpConstants instance.
						 By default a new one is created.
		:return: cpDBedit instance.
		"""

		self.filename = filename
		self.dbeditdata = []
		self.constants = constants


	def __str__(self):
		"""
		Print instance content in human redable format: [{<item1>}, {item2}, ...].
		"""

		return json.dumps([entry['item'].dict for entry in self.dbeditdata], indent=4)


	def gen_extract(self):
		"""
		Generator fucntions. Extracts data from dbedit file and return:
		  - 'id' - name for objects and rule number for rules/sections.
		  - 'nested_key' - used to build items definitions in R80.X API JSON format,
		  - 'value',
		  - 'type'.

		Two groups are extracted from dbedit files using regular expressions: 'id', 'value'. Two others are taken from cpConstants definitions for each regex.
		If there is no specific value in a dbedit line (no regex group), 'value' = 'id' in returned data.

		:yield: (<id>, <nested_key>, <value>, <type>) tuple.
		"""

		with open(self.filename, newline='') as dbedit_file:

			# Read each line.
			for line in dbedit_file:

				# Match all regex expression until first match.
				for match_dict in self.constants.regex_dbedit:

					match = re.match(match_dict['regex'], line)

					# There is a match.
					if match:

						matched_groups = match.groups()

						# 'id' should always be present.
						id = matched_groups[0]

						# Only 'id' present or also distinct 'value' matched.
						if len(matched_groups) == 1:
							val = id
						else:
							val = matched_groups[1]

						# Check all keys.
						for key in match_dict['keys']:

							# Value in a dict for specific key.
							if val in match_dict['keys'][key]:
								ret_val = match_dict['keys'][key][val]

							# Value not in a dict for specific key.
							else:
								if None in match_dict['keys'][key]:
									ret_val = match_dict['keys'][key][None]
								else:
									ret_val = val

							# Yield result if returned value (ret_val) is not 'None'.
							if not ret_val is None:
								yield (id, key, int(ret_val) if match_dict.get('int') else ret_val.strip() if isinstance(ret_val, str) else ret_val, match_dict['type'])

						# Stop checking other matches.
						if match_dict['stop']:
							break


	def item(self, id):
		"""
		Return item based on id. If item is not found return empty dict.

		:param id: Entry id.
		:return: Item dict.
		"""

		for entry in self.dbeditdata:
			if entry['id'] == id:
				return entry['item']
		return {}


	def read(self):
		"""
		Read dbedit file and save results in the buffer (dbeditdata).
		"""

		# Extract data from dbedit file.
		for (id, key, val, type) in self.gen_extract():

			item = self.item(id)

			# Item already exists - update.
			if item:

				# Proceed only if matched type is the same as in the item or is 'None' (don't care).
				if type == item['type'] or type is None:

					# Key exists in item - value must be a list. If needed create one.
					if key in item:
						if isinstance(item[key], list):
							item[key].append(val)
						else:
							item[key] = [item[key], val]

					# Key not found in item - simply add value.
					else:
						item[key] = val

			# Item doesn't exist - create new entry.
			else:
				if type:
					item = NestedDict({'type': type})
					item[key] = val
					self.dbeditdata.append({'id': id, 'item': item})


	def save(self, cpdata, container=None):
		"""
		Save local data to a cpData instance or its children instances.

		:param cpdata: cpData instance or its children instances.
		:param container: (optional) Define package/layer if saving rules.
		"""

		if container:
			cpdata.add([entry['item'].dict for entry in self.dbeditdata], container)
		else:
			cpdata.add([entry['item'].dict for entry in self.dbeditdata])


	def clear(self):
		"""
		Clear local buffer.
		"""
		self.dbeditdata = []
