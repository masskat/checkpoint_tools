from cptools import cpObjects
from cptools import cpRulebase
from cptools import cpWebAPI
from cptools import cpConstants
from cptools import cpCSV
from cpapi import APIClient, APIClientArgs
import json
import getpass
import argparse

#
# cp-csv-export.py v1.0
#
# requirements:
#  - Python 3.3
#  - Check Point API SDK (https://github.com/CheckPointSW/cp_mgmt_api_python_sdk
#  - Check Point tools (https://bitbucket.org/masskat/checkpoint_tools)
#  - NestedDict module (https://bitbucket.org/masskat/nested_dict/)
#

descr1 = """
Export Check Point configuration (objects, access and NAT rulebases) via Web API to a CSV file.
For Check Point R80 and above!
"""

descr2 = """
CSV file syntax:
  
  action*;type;<param1>;<param2>;<paramN>
 
  parameters:
    - action* - always set to add
    - type - item type (e.g. host, access-rule, etc.), according to the API documentation
    - <paramN> - specific parameter according to the API documentation
  
  Semicolon (;) is used for separation in output CSV files. Lists of values are separated by coma (,).
  Nested parameters are separated by dots (.), (e.g. nat-settings.auto-rule).
  
  Values are generally treated as strings except following cases:
    - numbers - by default considered as integers; placed within single quotes, e.g. '53' if treated as strings
    - True/False - these special values are treated as booleans
    
example:

  PS > python .\cp-csv-export.py -o network -o service 192.168.38.71
  
  Enter 192.168.38.71 SMS credentials.
  Username: admin
  Password:
  
  Downloading objects:
   host: 1/1
   network: 8/8
   address-range: 2/2
   group: 2/2
   group-with-exclusion: 0/0
   service-tcp: 50/217
   service-tcp: 100/217
   service-tcp: 150/217
   service-tcp: 200/217
   service-tcp: 217/217
   service-udp: 50/96
   service-udp: 96/96
   service-icmp: 13/13
   service-icmp6: 24/24
   service-sctp: 0/0
   service-other: 42/42
   service-dce-rpc: 41/41
   service-rpc: 18/18
   service-group: 50/54
   service-group: 54/54
  
  List of objects UIDs that couldn't be translated:
  []
 
"""

# Handle input arguments.
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=descr1, epilog=descr2)
parser.add_argument('-o', '--objects', metavar='CATEGORY', action='append', help='object category: network, service (multiple categories can be used)')
parser.add_argument('-l', '--layer', metavar='LAYER', help='policy layer name (used to export an access rulebase)')
parser.add_argument('-p', '--package', metavar='PACKAGE', help='policy package name (used to export a NAT rulebase)')
parser.add_argument('-x', '--prefix', metavar='PREFIX', default='', help='output file prefix')
parser.add_argument('--ignore_warnings', action='store_true', default=False, help='ignore API warnings')
parser.add_argument('mgmt', metavar='HOST', help='Management Server IP or hostname')
args = parser.parse_args()

# Get SMS credentials.
print('\nEnter ' + args.mgmt + ' SMS credentials.')
usr = input('Username: ')
pwd = getpass.getpass()

# API communication.
with APIClient(APIClientArgs(server=args.mgmt)) as client:

    # Check server's fingerprint.
    if client.check_fingerprint() is False:
        print("Could not get the server's fingerprint - exiting.")
        exit(1)

    # Login.
    res = client.login(usr, pwd)

    # Login to SMS successful.
    if res.success:

        wapi = cpWebAPI(client)
        objects = cpObjects()
        rulebase = cpRulebase()
        constants = cpConstants()

        # Get Objects.
        if args.objects:
            print('\nDownloading objects:')
            for type in constants.types(args.objects):
                for res in wapi.gen_get_objects(type, details_level='full'):
                    print(' ' + type + ': ' + str(res))
                if wapi.apiresponse.success:
                    wapi.save_objects(objects)
                else:
                    raise Exception('Problem with dowloading ' + type + ' objects:\n' + wapi.apiresponse.error_message)

            # Translate objects and write results.
            print('\nList of objects UIDs that couldn\'t be translated:\n' + str(objects.translate()))

            # Save objects to CSV.
            csvobjects = cpCSV(args.prefix + 'objects.csv')
            for name in objects.objects():
                csvobjects.newline(*objects.crop(name), 'add')
            csvobjects.write()

        # Get access/nat-rulebase.
        for (rulebase_type, container) in [('access-rulebase', args.layer), ('nat-rulebase', args.package)]:

            # Proceed only if particular container (layer, package) is set.
            if container:
            
                print('\nDownloading ' + rulebase_type + ':')
                for res in wapi.gen_get_rulebase(rulebase_type, container, 'full'):
                    print(' ' + rulebase_type + ': ' + str(res))

                # Download successful.
                if wapi.apiresponse.success:

                    # Save rulebase to a cpRulebase object and translate names.
                    wapi.save_rulebase(container, rulebase)
                    rulebase.translate(True, '')

                    # Save rules to CSV.
                    csvrulebase = cpCSV(args.prefix + rulebase_type + '.csv')
                    for crop_tuple in rulebase.gen_crop(container):
                        csvrulebase.newline(crop_tuple[0], crop_tuple[1], 'add', {'position': 'bottom'})
                    csvrulebase.write()

                # Download unsuccessful.
                else:
                    raise Exception('Problem with downloading ' + rulebase_type + ':\n' + str(wapi.apiresponse.error_message))

    # Failed to login.
    else:
        print('\nFail to login:\n' + str(res.error_message))
