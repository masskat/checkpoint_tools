from cptools import cpCSV
from cptools import cpWebAPI
from cpapi import APIClient, APIClientArgs
import cptools.cptools_functions as functions
import json
import getpass
import argparse

#
# cp-csv-import.py v1.0
#
# requirements:
#  - Python 3.3
#  - Check Point API SDK (https://github.com/CheckPointSW/cp_mgmt_api_python_sdk
#  - Check Point tools (https://bitbucket.org/masskat/checkpoint_tools)
#  - NestedDict module (https://bitbucket.org/masskat/nested_dict/)
#

descr1 = """
Import Check Point configuration via Web API from a CSV file.
Changes are automatically published if all changes are successfuly applied.
For Check Point R80 and above!
"""

descr2 = """
CSV file syntax:
  
  action*;type;<param1>;<param2>;<paramN>
 
  parameters:
    - action* (mandatory) - add, set, delete
    - type (mandatory) - item type (e.g. host, access-rule, etc.), according to the API documentation
    - <paramN> - specific parameter according to the API documentation
  
  CSV files must use semicolon (;) for separation. Lists of values are separated by coma (,).
  Nested parameters are separated by dots (.), (e.g. nat-settings.auto-rule).
  API request is derived from the action* and type params in the following way: action* + '-' + type (e.g. add-host).
  
  Values are generally treated as strings except following cases:
    - numbers - by default considered as integers; if a number must be treated as a string, use single quotes, e.g. '53'
    - True/False - these special values are treated as booleans

  Empty values are omitted. You can use different types and actions in a single CSV file.
  In such case define all paremeters in a CSV file header but leave values empty for parames not used within specific type.

example:

  test1.csv:
  action*;type;layer;rule-number;install-on;name;ip-address;nat-settings.auto-rule;nat-settings.ip-address
  set;access-rule;Network;1;cpgw2;;;;
  set;access-rule;Network;2;cpgw2;;;;
  add;host;;;;host1;10.1.1.1;True;10.2.2.2
  add;host;;;;host2;10.2.2.2;False;

  PS > python .\cp-csv-import.py --ignore_warnings 192.168.38.71 test1.csv

  Enter 192.168.38.71 SMS credentials.
  Username: admin
  Password:
  
  [ OK ] set-access-rule:
  {
      "layer": "Network",
      "rule-number": 1,
      "install-on": "cpgw2"
  }
  
  [ OK ] set-access-rule:
  {
      "layer": "Network",
      "rule-number": 2,
      "install-on": "cpgw2"
  }
  
  [ OK ] add-host:
  {
      "name": "host1",
      "ip-address": "10.1.1.1",
      "nat-settings": {
          "auto-rule": true,
          "ip-address": "10.2.2.2"
      }
  }
  
  [ OK ] add-host:
  {
      "name": "host2",
      "ip-address": "10.2.2.2",
      "nat-settings": {
          "auto-rule": false
      }
  }
  
  Publishing changes... Done
 
"""

# Handle input arguments.
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=descr1, epilog=descr2)
parser.add_argument('--ignore_warnings', action='store_true', default=False, help='ignore API warnings')
parser.add_argument('mgmt', metavar='HOST', help='Management Server IP or hostname')
parser.add_argument('csv', metavar='CSV_FILE', help='CSV filename')
args = parser.parse_args()

# Read CSV file.
csvobj = cpCSV(args.csv)
csvobj.read()

# General status.
success = True

# Convert boolean statuses to strings.
status_dict = {
    True: '[ OK ]',
    False: '[FAIL]'
}

# Get SMS credentials.
print('\nEnter ' + args.mgmt + ' SMS credentials.')
usr = input('Username: ')
pwd = getpass.getpass()

# API communication.
with APIClient(APIClientArgs(server=args.mgmt)) as client:

    # Check server's fingerprint.
    if client.check_fingerprint() is False:
        print("Could not get the server's fingerprint - exiting.")
        exit(1)

    # Login to SMS.
    login_res = client.login(usr, pwd)

    # Login to SMS successful.
    if login_res.success:

        wapi = cpWebAPI(client)

        # Process CSV data, line by line.
        for line in csvobj.csvdata:
        
            # Action/type.
            act = line['action']
            tp = line['item'].pop('type', None)

            # Action and type defined - continue (API call + display result).
            if act and tp:
                res = wapi.post(tp, line['item'], act, {'ignore-warnings': args.ignore_warnings})
                print('\n' + status_dict[res.success] + ' ' + act + '-' + tp + ':\n' + json.dumps(line['item'], indent=4))
                if not res.success:
                    success = False
                    print(res.error_message)
                    
             # Action or type not defined - return error message.                  
            else:
                success = False
                print('\n' + 'Action/type not defined for:\n' + json.dumps(line['item'], indent=4))

        # Publish changes (only if all API calls were successful).
        functions.api_publish(wapi, success)

    # Failed to login.
    else:
        print('\nFail to login:\n' + str(login_res.error_message))
   
