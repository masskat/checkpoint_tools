import setuptools
from setuptools import setup

setup(
    name="cptools",
    version="1.0.0",
    author="masskat",
    author_email="masskat@example.com",
    description="Check Point API tools",
    long_description="Python 3 Check Point API tools.",
    long_description_content_type="text",
    url="https://bitbucket.org/masskat/checkpoint_tools.git",
    packages=setuptools.find_packages(),
    install_requires=['cpapi', 'ndict'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.3.*',
)
