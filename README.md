# cptools
Python 3 tools for processing Check Point configuration via Web API.
Supports Check Point R80.10 and higher (API version >= v1.1).

Classes:

* **cpData**: Generic class representing Check Point configuration

* **cpRulebase**: Check Point rulebase handling (cpData child)

* **cpObjects**: Check Point objects handling (cpData child)

* **cpWebAPI**: Web API interface

* **cpCSV**: Processing CSV files

* **cpDBedit**: Parsing DBedit configuration

* **cpConstants**: Definitions of specific Check Point configuration items

* **cpException**: Exceptions handling

cpConstants

## Requirements

Library requires **ndict** package. It can be installed in the following way:
```
pip install git+https://bitbucket.org/masskat/nested_dict
```

For API communication it uses **cpapi** library available at:
```
pip install git+https://github.com/CheckPointSW/cp_mgmt_api_python_sdk
```


## Installation

### Cloning the repository:
```
git clone https://bitbucket.org/masskat/checkpoint_tools.git
```

### Installing directly from the repository using pip
```
pip install git+https://bitbucket.org/masskat/checkpoint_tools
```

### Upgrading directly from the repository using pip
```
pip install --upgrade git+https://bitbucket.org/masskat/checkpoint_tools
```

### Local installation with pip
Download the archive, unpack it, navigate to the repository directory and run:
```
pip install .
```

### Uninstalling
```
pip uninstall cptools
```

## Development Environment
The library was developed using Python 3.8.
